/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.ketoan;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class KetoanController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
     @FXML
    private void handleBackToLoginAction(ActionEvent event) {
    try {
        // Load the login scene FXML
        Parent loginRoot = FXMLLoader.load(getClass().getResource("/g1/quanlythuoctay/login.fxml"));

        // Get the current stage from the event source
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        // Set the login scene to the stage
        stage.setScene(new Scene(loginRoot));
        stage.show();
    } catch (IOException e) {
        e.printStackTrace();
        // handle exception
    }
}
}
