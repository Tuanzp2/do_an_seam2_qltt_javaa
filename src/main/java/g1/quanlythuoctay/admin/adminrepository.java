/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay.admin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class adminrepository {
    private String url = "jdbc:sqlserver://127.0.0.1:1433; databaseName = QLTT; encrypt=true; trustServerCertificate=true";
    private String username = "kynv";
    private String password = "123";
    private Connection conn;
    public adminrepository(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn=DriverManager.getConnection(url,username,password);
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
        }catch(SQLException e){
            e.printStackTrace();
        }       
    }
    
    public ObservableList<admin> findAll() {
        ObservableList<admin> ls = FXCollections.observableArrayList();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT NhanVien.MaNV, NhanVien.TenNV, NhomQuyen.MaVaiTro, NhomQuyen.MaNhomQuyen, NhomQuyen.TenNhomQuyen\n" +
"FROM NhanVien\n" +
"JOIN NhomQuyen ON NhanVien.MaNV = NhomQuyen.MaNV;");
            while (rs.next()) {
                admin item = new admin();
                item.setMaNV(rs.getString("MaNV"));
                item.setTenNV(rs.getString("TenNV"));
                item.setMaVaiTro(rs.getInt("MaVaiTro"));
                item.setMaNhomQuyen(rs.getInt("MaNhomQuyen"));
                item.setTenNhomQuyen(rs.getString("TenNhomQuyen"));
                                          
                ls.add(item);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            // Handle the exceptions, e.g., by logging or throwing an error.
        }
        return ls;
    }
}
