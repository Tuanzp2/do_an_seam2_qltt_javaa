/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay.admin;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class AdminController implements Initializable {

    /**
     * Initializes the controller class.
     */
 @FXML

private GridPane gridpane;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       adminrepository mcr = new adminrepository();

ObservableList<admin> ls = mcr.findAll();

 int row = 1;
        
        for (admin item : ls) {
            Label MaNV = new Label(  item.getMaNV());
            Label TenNV=new Label(item.getTenNV());
            Label MaVaiTro = new Label( ""+item.getMaVaiTro());
            Label MaNhomQuyen = new Label(""+item.getMaNhomQuyen(row));
            Label TenNhomQuyen = new Label(item.getTenNhomQuyen());
                 
          
            // Add more labels for other columns as needed
            
            gridpane.add(MaNV, 0, row);
            gridpane.add(TenNV, 1, row);
             gridpane.add(MaVaiTro, 2, row);
              gridpane.add(MaNhomQuyen, 3, row);
              gridpane.add(TenNhomQuyen, 4, row);
              
            
            // Add more labels to GridPane with appropriate column and row values
            
            row++;
        }
    }
      
   @FXML
    private void handleBackToLoginAction(ActionEvent event) {
    try {
        // Load the login scene FXML
        Parent loginRoot = FXMLLoader.load(getClass().getResource("/g1/quanlythuoctay/login.fxml"));

        // Get the current stage from the event source
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        // Set the login scene to the stage
        stage.setScene(new Scene(loginRoot));
        stage.show();
    } catch (IOException e) {
        e.printStackTrace();
        // handle exception
    }
}
 
}
