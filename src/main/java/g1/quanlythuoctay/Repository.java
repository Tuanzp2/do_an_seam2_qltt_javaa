/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package g1.quanlythuoctay;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author MININT-QHS5KT4-local
 */
public class Repository {
    
    private String url = "jdbc:sqlserver://127.0.0.1:1433; databaseName = QLTT; encrypt=true; trustServerCertificate=true";
    private String username = "kynv";
    private String password = "123";
    private Connection conn;
    public Repository(){
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn=DriverManager.getConnection(url,username,password);
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public User validateUser(String username, String password) {
    User validUser = null;
    try {
        // It's better to use a PreparedStatement to protect against SQL injection
        String query = "SELECT * FROM NhanVien WHERE UserName = ? AND MatKhau = ?";
        PreparedStatement stmt = conn.prepareStatement(query);
        
        // Set the parameters
        stmt.setString(1, username);
        stmt.setString(2, password); // This should be a hashed password
        
        ResultSet rs = stmt.executeQuery();

        // If user is found
        if (rs.next()) {
            validUser = new User();
            validUser.setMaNV(rs.getString("MaNV"));
            validUser.setTenNV(rs.getString("TenNV"));
            validUser.setMaKho(rs.getString("MaKho"));
            validUser.setUserName(rs.getString("UserName"));
            validUser.setMatKhau(rs.getString("MatKhau"));
            validUser.setMaca(rs.getInt("Maca"));
            // Found user with matching credentials
        }
    } catch (SQLException e) {
        // Handle exception
        String err = e.getMessage();
        e.printStackTrace(); // It's better to log errors
    }
    return validUser;
}
    
    public String getUserRole(User user) {
    String role = null;
    if (user == null || user.getMaNV() == null) {
        return null; // Or throw an IllegalArgumentException
    }
    String query = "SELECT TenNhomQuyen FROM NhomQuyen WHERE MaNV = ?";
    try (PreparedStatement stmt = conn.prepareStatement(query)) {
        // Set the parameter for the MaNV
        stmt.setString(1, user.getMaNV());
        try (ResultSet rs = stmt.executeQuery()) {
            // If a role is found, set it to the role variable
            if (rs.next()) {
                role = rs.getString("TenNhomQuyen");
            }
        }
    } catch (SQLException e) {
        // Handle exception
        e.printStackTrace(); // It's better to log errors
    }
    return role;
}
}

