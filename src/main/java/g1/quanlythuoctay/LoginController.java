/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package g1.quanlythuoctay;

import static g1.quanlythuoctay.App.primaryStage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author MININT-QHS5KT4-local
 */
public class LoginController implements Initializable {
@FXML
TextField tf_user;
@FXML
TextField tf_password;
@FXML
Button bt_login;
@FXML
Label lb_output;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bt_login.addEventHandler(ActionEvent.ACTION, eh->{
        String username = tf_user.getText();
        String password = tf_password.getText(); // This should be hashed before comparing
        Repository repo = new Repository();
        User user = repo.validateUser(username, password);

        if (user != null) {
        lb_output.setText("Xin chao: "+user.getTenNV());
         switchSceneBasedOnRole(repo.getUserRole(user));
        } else {
        lb_output.setText("Invalid user or password for: " +username);
        }
       }); 
    }    
   private void switchSceneBasedOnRole(String role) {
        try {
            FXMLLoader loader = new FXMLLoader();
            switch (role) {
                case "admin":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/admin/admin.fxml"));
                    break;
                case "quanlykinhdoanh":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/quanlykinhdoanh/quanlykinhdoanh.fxml"));
                    break;
                case "kinhdoanh":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/kinhdoanhkiemkho/kinhdoanhkiemkho.fxml"));
                    break;
                 case "kiemkho":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/kinhdoanhkiemkho/kinhdoanhkiemkho.fxml"));
                    break;
                case "banthuoc":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/banthuoc/banthuoc.fxml"));
                    break;
                case "ketoan":
                    loader.setLocation(getClass().getResource("/g1/quanlythuoctay/ketoan/ketoan.fxml"));
                    break;
                // Add cases for other roles as necessary
                default:
                    // handle unknown role
                    return;
            }
            Parent root = loader.load();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
            // handle exception
        }
    } 
}
