module g1.quanlythuoctay {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;
    requires com.microsoft.sqlserver.jdbc;
    requires java.sql;
    opens g1.quanlythuoctay to javafx.fxml;
    exports g1.quanlythuoctay;
    opens g1.quanlythuoctay.admin to javafx.fxml;
    exports g1.quanlythuoctay.admin;
    opens g1.quanlythuoctay.banthuoc to javafx.fxml;
    exports g1.quanlythuoctay.banthuoc;
    opens g1.quanlythuoctay.ketoan to javafx.fxml;
    exports g1.quanlythuoctay.ketoan;
    opens g1.quanlythuoctay.quanlykinhdoanh to javafx.fxml;
    exports g1.quanlythuoctay.quanlykinhdoanh;
    opens g1.quanlythuoctay.kinhdoanhkiemkho to javafx.fxml;
    exports g1.quanlythuoctay.kinhdoanhkiemkho;
}
